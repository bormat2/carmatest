"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.encrypt = encrypt;
exports.decrypt = decrypt;

// import * as crypto from 'crypto'
const crypto = require('crypto');

const algorithm = 'aes-256-ctr';
const ENCRYPTION_KEY = 'mypaqmypaqkqsmlk#<qmrdkqsmlkDSdq'; // Must be 256 bytes (32 characters)

const IV_LENGTH = 16; // For AES, this is always 16

function encrypt(text) {
  let iv = crypto.randomBytes(IV_LENGTH);
  let cipher = crypto.createCipheriv(algorithm, new Buffer(ENCRYPTION_KEY), iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return iv.toString('hex') + ':' + encrypted.toString('hex');
}

function decrypt(text) {
  let textParts = text.split(':');
  let iv = new Buffer(textParts.shift(), 'hex');
  let encryptedText = new Buffer(textParts.join(':'), 'hex');
  let decipher = crypto.createDecipheriv(algorithm, new Buffer(ENCRYPTION_KEY), iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
}

module.exports = {
  decrypt,
  encrypt
}; // let a = encrypt('5204416916790646')
// let b = encrypt('6011256314347016')
// console.log(a)
// console.log(b)
// console.log(encrypt('123'))
// console.log(encrypt('999'))
// console.log( decrypt(a) === '5204416916790646')
// console.log( decrypt(b) === '6011256314347016')