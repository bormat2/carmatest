"use strict";

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

const _require = require('./database'),
      getClient = _require.getClient;

describe('Database tests', function () {
  it('simple select',
  /*#__PURE__*/
  _asyncToGenerator(function* () {
    const client = yield getClient();
    const resQuer = yield client.query(`
            SELECT count(*) as nb_lines from credit_card
        `);
    expect(+resQuer[0].nb_lines).toBeGreaterThanOrEqual(0);
  }));
});