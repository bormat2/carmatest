"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getClient = void 0;

var _utilsFunc = require("./utilsFunc");

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

const _require = require('pg'),
      Pool = _require.Pool; // const named = require('node-postgres-named');


const pool = new Pool({
  user: 'me',
  host: '127.0.0.1',
  database: 'carma_test',
  password: 'Carm@TestPass',
  port: 5432
}); // To accept the same syntax that PDO in PHO with name parameter
// we replace parameter object by an array of parameters
// get keys starting by longuer key to avoid error 
// if a small key is inside a bigger key when we will replace

const nameParameterToNumber = function (sql
/*string*/
, parameter
/*object*/
) {
  const keys = Object.keys(parameter || {}).sort((a, b) => b.length - a.length);
  let sqlCorrected = sql; // sql with $1 and $2 instead of :name1 and :name2

  let paramsArray = []; // parameter at position 0 is $1 and at positin 1 is $2

  keys.forEach((key, i) => {
    const escapedStr = (0, _utilsFunc.escapeRegExp)(':' + key);
    const reg = new RegExp(escapedStr, 'g');
    sqlCorrected = sqlCorrected.replace(reg, '$' + (i + 1));
    paramsArray[i] = parameter[key];
  });
  return {
    sqlCorrected,
    paramsArray
  };
};

console.log('@TEST:', nameParameterToNumber('select :toto, 0, :tototu', {
  tototu: 'a2',
  toto: 'a1'
}));

class Client {
  constructor(client) {
    throw 'use Client.getClient instead';
  } // execute a request with named parameters and get the postgres 
  // object as result


  _exec(sql, parameter) {
    var _this = this;

    return _asyncToGenerator(function* () {
      const _nameParameterToNumbe = nameParameterToNumber(sql, parameter),
            sqlCorrected = _nameParameterToNumbe.sqlCorrected,
            paramsArray = _nameParameterToNumbe.paramsArray;

      console.log('sqlCorrected:', sqlCorrected, 'paramsArray:', paramsArray);
      let res = yield _this.pgClient.query(sqlCorrected, paramsArray);
      return res;
    })();
  }

  query(sql, parameter) {
    var _this2 = this;

    return _asyncToGenerator(function* () {
      try {
        let res = yield _this2._exec(sql, parameter);
        return res.rows;
      } catch (e) {
        console.error(e);
        return {
          error: e.message
        };
      }
    })();
  }

  exec(sql, parameter) {
    var _this3 = this;

    return _asyncToGenerator(function* () {
      try {
        yield _this3._exec(sql, parameter).rows;
      } catch (e) {
        console.error(e);
        return {
          error: e.message
        };
      }

      return {
        success: true
      };
    })();
  }

}

const getClient = Client.getClient =
/*#__PURE__*/
_asyncToGenerator(function* () {
  let pgClient;

  try {
    pgClient = yield pool.connect();
  } catch (e) {
    console.error('connection to postgres failed');
    throw e;
  }

  const client = Object.create(Client.prototype); // now can can use ':name' instead of '$1' in queries
  // named.patch(client);

  client.pgClient = pgClient;
  return client;
});

exports.getClient = getClient;