"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _path = _interopRequireDefault(require("path"));

var https = _interopRequireWildcard(require("https"));

var http = _interopRequireWildcard(require("http"));

var _glob = _interopRequireDefault(require("glob"));

var _utilsFunc = _interopRequireDefault(require("./modules/utilsFunc"));

var _database = require("./modules/database");

var _cryptog = require("./modules/cryptog");

var _backFrontUtils = require("./backAndFront/backFrontUtils");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

// import bodyParser from 'body-parser'
const formidable = require('express-formidable'); // prod_is_active is true if react and node are on same port because react is built


const startServer = function (prod_is_active) {
  // listening for uncaughtException must be done at soon as possible
  process.on('uncaughtException', function (err) {
    console.log(err); // in dev mode stop node at the first error to help to read errors 

    if (!prod_is_active) {
      process.exit();
    }
  }); // we will only listen request if they contain root_url at the begining

  const root_url = '/serverRoot/';
  const port = 5555;
  const app = (0, _express.default)(); // In development mode react is not on the same port that the node server
  // we have to allow cross origin for the wanted port
  // if(!prod_is_active){

  console.warn('You are using the dev mode, where we allows CROSS');
  app.use(function (req, res, next) {
    // react_server is the server that run the frontend
    // react_server is only used if prod_is_active = false
    const react_server = prod_is_active ? "http://localhost:" + port : "http://localhost:3000";
    res.header("Access-Control-Allow-Origin", react_server);
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Credentials", 'true');
    next();
  }); // }else{
  //     console.log('You are in prod mode')
  // }

  const routes_listen = [];
  init_server(port, root_url).then(function () {
    // console.log(routes_listen)
    routes_listen.forEach(obj => {
      app.get(obj.url, obj.func);
    });
  });
  return;
  /****************************************************************************
   ***** After this comment code is not executed, there is only functions *****
   ****************************************************************************
  */
  // defines routes accessibles with get method 

  function get_route(url) {
    const listen_url = _path.default.join(root_url, url); // console.log('listen get : ', listen_url)


    routes_listen.push({
      url,
      func: arguments.length <= 1 ? undefined : arguments[1]
    }); // return app.get(listen_url, ...args)
  } // defines routes accessibles with post method 


  function post_route(url) {
    const listen_url = _path.default.join(root_url, url);

    console.log('listen post : ', listen_url);

    for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
      args[_key - 1] = arguments[_key];
    }

    return app.route(listen_url).post(...args);
  } // get a path inside the current folder 


  function rel_path(path_str) {
    return _path.default.join(__dirname, path_str);
  }

  function acceptPayment(_x, _x2, _x3) {
    return _acceptPayment.apply(this, arguments);
  } // defines all routes and start the server


  function _acceptPayment() {
    _acceptPayment = _asyncToGenerator(function* (req, res, next) {
      const inputs = JSON.parse(req.fields.data).input_params;
      const errors = (0, _backFrontUtils.controlForm)({ ...inputs,
        fromNode: true
      });

      if (Object.keys(errors).length) {
        res.status(422).send({
          message: 'There is error in the form',
          errors
        });
      }

      const credit_card = inputs.credit_card,
            cvv = inputs.cvv,
            expiration_date = inputs.expiration_date,
            holder_name = inputs.holder_name;
      const db = yield (0, _database.getClient)();

      try {
        yield db.exec(`
                INSERT INTO credit_card(digits, cvv, holder_name, expiration_date)
                VALUES (:credit_card, :cvv, :holder_name, :expiration_date)
            `, {
          cvv: (0, _cryptog.encrypt)(cvv),
          // this is totally forbidden to store a CVV in reality
          credit_card: (0, _cryptog.encrypt)(credit_card),
          expiration_date,
          holder_name
        });
        const resQuer = yield db.query(`
                SELECT count(*) as nb_lines from credit_card
            `); // console.log('res',resQuer.rows)

        const _resQuer = _slicedToArray(resQuer, 1),
              nbLines = _resQuer[0].nb_lines;

        res.send({
          message: 'Your payment has been accepted, there is currently ' + nbLines + ' card(s) store in our database'
        });
      } catch (e) {
        res.status(400).send({
          message: 'Database insertion error'
        });
        console.error(e);
        return;
      }
    });
    return _acceptPayment.apply(this, arguments);
  }

  function init_server(_x4, _x5) {
    return _init_server.apply(this, arguments);
  }

  function _init_server() {
    _init_server = _asyncToGenerator(function* (port, root_url) {
      // decode form data
      app.use(formidable()); // if the file exists in the FrontEnd folder in Production mode
      // redirect to this file

      if (prod_is_active) {
        const base = dist_main('');

        function redirectToReact() {
          return new Promise(function (resolve) {
            (0, _glob.default)(dist_main('**'), function (er, names) {
              console.log(1); // remove the part of the path that concern the react folder
              // and create the route

              names.forEach(absolute_path => {
                const relative_path = absolute_path.slice(base.length - 1); // do not match '' because it will match all route

                if (relative_path === '') return;
                get_route(relative_path, function (req, res) {
                  res.sendFile(absolute_path);
                });
              });
              resolve();
            });
          });
        }

        yield redirectToReact();
      }

      post_route('api/pay', acceptPayment);
      get_route('*', function (req, res) {
        // console.log('unknow route',req.url)
        // redirect trafic to react if there is no matching urls
        res.sendFile(dist_main('index.html')); // res.end('This route with Get Method does not exist : '+req.url)
      });
      post_route('*', function (req, res) {
        res.status(404).end('This route ' + req.url + ' with Post Method does not exist');
      });
      const server = app.listen(port, function () {
        console.log('Listening on port %d', server.address().port);
      });

      function dist_main(url) {
        return rel_path('../lib/FrontEnd/') + url;
      }
    });
    return _init_server.apply(this, arguments);
  }
};

var _default = startServer;
exports.default = _default;