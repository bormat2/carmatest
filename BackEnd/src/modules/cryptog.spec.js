const {encrypt, decrypt} = require('./cryptog.js')

describe('Encrypt and Decrypt', function() {
    describe('encryption worked', function() {
        const a_before = '5204416916790646'
        const b_before = '6011256314347016'
        const a = encrypt(a_before);
        const b = encrypt(b_before);

        // Edit: in fact there is several way to encode the same message, so it is almost never the same result
        // it('value encrypted can not changed', function(){
        //     expect(a).toEqual('6633cfc55b12996b1e6d73d63d8f4347:4ed9a9c445ec9d85a1b4ed1519f7c7a3')
        // })

        it('encrypted length must not change', function(){
            expect(a.length).toEqual("6633cfc55b12996b1e6d73d63d8f4347:4ed9a9c445ec9d85a1b4ed1519f7c7a3".length)
        })
        it('a and b must be different', function(){
            expect(a).not.toEqual(a_before)
            expect(b).not.toEqual(b_before)
        });

        it('decrypt must work', function(){
            expect(decrypt(a)).toEqual(a_before);
            expect(decrypt(b)).toEqual(b_before);
        })
    })
})