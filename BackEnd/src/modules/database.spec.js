const {getClient} = require('./database');

describe('Database tests', function() {
    it('simple select', async function(){
        const client = await getClient()
        const resQuer = await client.query(`
            SELECT count(*) as nb_lines from credit_card
        `)
        expect(+resQuer[0].nb_lines).toBeGreaterThanOrEqual(0)
    })
})