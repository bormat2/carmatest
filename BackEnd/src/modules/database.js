import {escapeRegExp} from './utilsFunc'
const { Pool } = require('pg')

// const named = require('node-postgres-named');


const pool = new Pool({
    user: 'me',
    host: '127.0.0.1',
    database: 'carma_test',
    password: 'Carm@TestPass',
    port: 5432,
});

// To accept the same syntax that PDO in PHO with name parameter
// we replace parameter object by an array of parameters
// get keys starting by longuer key to avoid error 
// if a small key is inside a bigger key when we will replace
const nameParameterToNumber = function(sql/*string*/, parameter/*object*/){
    const keys = Object.keys(parameter || {}).sort((a,b)=> b.length - a.length)
    let sqlCorrected = sql // sql with $1 and $2 instead of :name1 and :name2
    let paramsArray = [] // parameter at position 0 is $1 and at positin 1 is $2
    keys.forEach((key, i)=>{
        const escapedStr = escapeRegExp(':'+key)
        const reg = new RegExp(escapedStr,'g')
        sqlCorrected = sqlCorrected.replace(reg, '$'+(i+1))
        paramsArray[i] = parameter[key]
    })
    return {sqlCorrected, paramsArray}
}

console.log('@TEST:',nameParameterToNumber('select :toto, 0, :tototu',{
    tototu: 'a2',
    toto: 'a1'
}))


class Client{
    constructor(client){
        throw 'use Client.getClient instead'
    }

    // execute a request with named parameters and get the postgres 
    // object as result
    async _exec(sql, parameter){
        const {sqlCorrected, paramsArray} = nameParameterToNumber(sql, parameter)
        console.log('sqlCorrected:',sqlCorrected, 'paramsArray:',paramsArray)
        let res = await this.pgClient.query(sqlCorrected, paramsArray)
        return res
    }

    async query(sql, parameter){
        try{
            let res = await this._exec(sql, parameter)
            return res.rows
        }catch(e){
            console.error(e)
            return {
                error: e.message
            }
        }
    }

    async exec(sql, parameter){
        try{
            await this._exec(sql, parameter).rows
        }catch(e){
            console.error(e)
            return {
                error: e.message
            }
        }
        return {
            success: true
        }
    }

}

const getClient = Client.getClient = async function(){
    let pgClient;
    try{
        pgClient = await pool.connect()
    }catch(e){
        console.error('connection to postgres failed')
        throw e
    }
    const client = Object.create(Client.prototype)
    // now can can use ':name' instead of '$1' in queries
    // named.patch(client);
    client.pgClient = pgClient
    return client
}


export {getClient}
