require('babel-register')({
    "presets": [
    [
      "env",
      {
        "targets": {
          "node": "current"
        }
      }
    ],
    "flow"
  ],
  "plugins": [
    "transform-es2015-destructuring",
    "transform-es2015-parameters",
    "transform-object-rest-spread"
  ]
})
// Import the rest of our application.
module.exports = require('./app.js')