"use strict"
import express from 'express' // Express Web Server
import path from 'path' // used for file path
// import * as SocketDef from './socket.io'
// import socketIO from './Socket.io/socket.io'
import * as https from 'https'
import * as http from 'http'
// import busboy from 'connect-busboy'
import glob from "glob"
import escapeRegExp from './modules/utilsFunc'
import {getClient} from './modules/database'
import {encrypt} from './modules/cryptog'

import {controlForm} from './backAndFront/backFrontUtils'
// import bodyParser from 'body-parser'
const formidable = require('express-formidable');

// prod_is_active is true if react and node are on same port because react is built
const startServer = function(prod_is_active){
    // listening for uncaughtException must be done at soon as possible
    process.on('uncaughtException', function (err) {
        console.log(err)
        // in dev mode stop node at the first error to help to read errors 
        if(!prod_is_active){
            process.exit()
        }
    })

    // we will only listen request if they contain root_url at the begining
    const root_url = '/serverRoot/'
    const port = 5555
    const app = express()
    
    // In development mode react is not on the same port that the node server
    // we have to allow cross origin for the wanted port
    // if(!prod_is_active){
    console.warn('You are using the dev mode, where we allows CROSS')
    app.use(function(req, res, next) {
        // react_server is the server that run the frontend
        // react_server is only used if prod_is_active = false
        const react_server = prod_is_active ? ("http://localhost:" + port) : "http://localhost:3000"
        res.header("Access-Control-Allow-Origin", react_server);
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        res.header("Access-Control-Allow-Credentials", 'true')
        next();
    });
    // }else{
    //     console.log('You are in prod mode')
    // }

    const routes_listen = []
    init_server(port, root_url).then(function(){
        // console.log(routes_listen)
        routes_listen.forEach((obj)=>{
            app.get(obj.url,obj.func)
        })
    })
    return; 


    /****************************************************************************
     ***** After this comment code is not executed, there is only functions *****
     ****************************************************************************
    */

    // defines routes accessibles with get method 
    function get_route(url, ...args){
        const listen_url = path.join(root_url, url)
        // console.log('listen get : ', listen_url)
        routes_listen.push({url, func: args[0] })
        // return app.get(listen_url, ...args)
    }

    // defines routes accessibles with post method 
    function post_route(url, ...args){
        const listen_url = path.join(root_url, url)
        console.log('listen post : ', listen_url)
        return app.route(listen_url).post( ...args)
    }

    // get a path inside the current folder 
    function rel_path(path_str){
        return path.join(__dirname, path_str)
    }

    async function acceptPayment(req, res, next) {
        const inputs = JSON.parse(req.fields.data).input_params
        const errors = controlForm({...inputs, fromNode:true})
        if(Object.keys(errors).length){
            res.status(422).send({
                message: 'There is error in the form',
                errors
            })
        }
        const {credit_card, cvv, expiration_date, holder_name} = inputs
        const db = await getClient()
        try{
            await db.exec(`
                INSERT INTO credit_card(digits, cvv, holder_name, expiration_date)
                VALUES (:credit_card, :cvv, :holder_name, :expiration_date)
            `,{
                cvv: encrypt(cvv),// this is totally forbidden to store a CVV in reality
                credit_card: encrypt(credit_card),
                expiration_date,
                holder_name
            })
            const resQuer = await db.query(`
                SELECT count(*) as nb_lines from credit_card
            `)
            // console.log('res',resQuer.rows)
            const [{nb_lines: nbLines}] = resQuer
            res.send({
                message: 'Your payment has been accepted, there is currently ' + nbLines + ' card(s) store in our database'
            })
        }catch(e){
            res.status(400).send({
                message: 'Database insertion error'
            })
            console.error(e)
            return;
        }
        
    }
    // defines all routes and start the server
    async function init_server(port, root_url){
        // decode form data
        app.use(formidable());

        // if the file exists in the FrontEnd folder in Production mode
        // redirect to this file
        if(prod_is_active){
            const base = dist_main('')
            function redirectToReact(){
                return new Promise(function(resolve){
                    glob(dist_main('**'),function(er, names){
                        console.log(1)
                        // remove the part of the path that concern the react folder
                        // and create the route
                        names.forEach((absolute_path)=>{
                            const relative_path = absolute_path.slice(base.length-1)
                            // do not match '' because it will match all route
                            if(relative_path === '') return;
                            get_route(relative_path, function(req, res){
                                res.sendFile(absolute_path);
                            })
                        })
                        resolve()
                    })
                })
            }
            await redirectToReact()
        }
        

        post_route('api/pay',acceptPayment)

        get_route('*', function(req, res) {
            // console.log('unknow route',req.url)
            // redirect trafic to react if there is no matching urls
            res.sendFile(dist_main('index.html'))
            // res.end('This route with Get Method does not exist : '+req.url)
        })

        post_route('*', function(req, res) {
            res.status(404).end('This route '+ req.url + ' with Post Method does not exist')
        })
      
        const server = app.listen(port, function() {
            console.log('Listening on port %d', server.address().port)
        })

        function dist_main(url){
            return rel_path( '../lib/FrontEnd/') + url
        }
    }

}

export default startServer
    