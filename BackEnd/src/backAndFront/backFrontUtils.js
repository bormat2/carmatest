/**
* This file must be compatible with react and node 
* so avoid features like async
*/


/**
* This algorithm return true if the value string look like
* a credit card and false otherwise
*
* @link: https://en.wikipedia.org/wiki/Luhn_algorithm
*/
function luhn_check(value) {
    return value.replace(/\s+/g, '').split('')
        .reverse()
        .map( (x) => parseInt(x, 10) )
        .map( (x,idx) => idx % 2 ? x * 2 : x )
        .map( (x) => x > 9 ? (x % 10) + 1 : x )
        .reduce( (accum, x) => accum += x ) % 10 === 0;
}

/**
* return errors in the form 
* the object return will have the field name as key and the error message as value
*/
const controlForm = function (inputs/* {[inputName]: inputVal} */) {
    const errors = {}
    const required = (val) => val ? null : 'This field is required'
    const checkOnlyDigitAndLength = (fieldName, length) => {
        let val = inputs[fieldName]
        let val2 = val && val.replace(/\s+/g, '')
        return required(val) ||
            /[^\d\s]/.test(val2) && "Only digits and number are allowed" ||
            (val2.length !== length) && ('The number of digits must be ' + length + ' instead of ' + val2.length)
    }
    // if there is an error add the error in errors
    const setErrorIfMessage = (field, message) => {if(message) errors[field] = message}
    
    let fieldName = 'credit_card'
    setErrorIfMessage(fieldName, checkOnlyDigitAndLength(fieldName,16) || 
        !luhn_check(inputs[fieldName]) && "The credit card does not seem to be correct"
    )

    fieldName = 'cvv'
    setErrorIfMessage(fieldName, checkOnlyDigitAndLength(fieldName,3))

    fieldName = 'holder_name'
    let val = inputs[fieldName]
    setErrorIfMessage(fieldName, required(val) || 
        val.length > 100 && "Your name is too long"
    )

    fieldName = 'expiration_date'
    val = inputs[fieldName]
    setErrorIfMessage(fieldName, required(val) || 
        !(/^((0[0-9])|(1[0-2]))\/2\d\d\d$/g).test(val) && "The date is not correct, the expected format is mm/YYYY"
    )

    return errors
};

export {
    controlForm
}
