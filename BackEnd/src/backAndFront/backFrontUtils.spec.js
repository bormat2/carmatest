const {controlForm} = require('./backFrontUtils')

const validData = Object.freeze({
    credit_card: '0000 0000 0000 0000',
    cvv: '000',
    holder_name: 'Bortolaso Mathieu',
    expiration_date: '08/2022'
})

describe('Array', function() {
    describe('valid data', function() {
        it('should return an empty object if there is no error', function(){
            expect(Object.keys(controlForm(validData)).length).toEqual(0)
        });

        it('should return an empty object if there is no error 2', function(){
            const data = {
                credit_card: '5500 0000 0000 0004',
                cvv: '456',
                holder_name: 'a'.repeat(100),
                expiration_date: '12/2999'
            }
            expect(Object.keys(controlForm(validData)).length).toEqual(0)
        });
    });

    describe('invalid data', function() {
        it('should  be required', function(){
            const data = {cvv: '', credit_card: '', holder_name: '', expiration_date: ''}
            expect(controlForm(data)).toEqual({
                "credit_card": "This field is required",
                "cvv": "This field is required",
                "expiration_date": "This field is required",
                "holder_name": "This field is required",
            })
        });

        it('should return errors', function(){
            const data = {cvv: '00', credit_card: '1', holder_name: validData.holder_name, expiration_date: '08/4040'}
            expect(controlForm(data)).toEqual({
                "credit_card": "The number of digits must be 16 instead of 1",
                "cvv": "The number of digits must be 3 instead of 2",
                "expiration_date": "The date is not correct, the expected format is mm/YYYY",
            })
        });

        it('should return errors 2', function(){
            const data = {cvv: '3333', credit_card: '1234567891123456', holder_name: 'A'.repeat(101), expiration_date: '13/2020'}
            
            expect(controlForm(data)).toEqual({
                "credit_card": "The credit card does not seem to be correct",
                "cvv": "The number of digits must be 3 instead of 4",
                "expiration_date": "The date is not correct, the expected format is mm/YYYY",
                "holder_name": "Your name is too long"
            })
        });
    });

});