const fs = require("fs-extra");

// relative path to package.json
const source = '../FrontEnd/build'
const destination = './lib/FrontEnd'
 
// copy source folder to destination
fs.copy(source, destination, function (err) {
    if (err){
        console.log('An error occured while copying the folder frontEnd to the lib folder.')
        return console.error(err)
    }
    console.log('React has been copied into lib')
});
