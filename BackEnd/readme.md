# [Carma_test](https://gitlab.com/bormat2/carma_server) &middot; [![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](#)

This application asks credit cards information and stores them in a Postgres database.

## Installation

### Prepare the database

In a terminal run

  `psql postgres`

create a user "me", thanks to this:

  `CREATE ROLE me WITH LOGIN PASSWORD 'Carm@TestPass';`

  `ALTER ROLE me CREATEDB;`

leave psql (CMD + d) and run

`psql -d postgres -U me`

and paste this

`CREATE DATABASE carma_test;`

`\c carma_test;`

`CREATE TABLE credit_card (`

`digits character varying(100) NOT NULL,--length of 16 after decoding`

`cvv character varying(50) NOT NULL,--length of 3 after decoding`

`id SERIAL,`

`expiration_date character varying(7) NOT NULL,`

`holder_name character varying(100) NOT NULL`

`);`
`

### Server installation
Now that the database is ready, we can install our nodeJs server

`npm i -s carma_test0_server`

And to run the server :

create index.js:
`const server = require('carma_test0_server').default;`

`server(true/*to avoid CORS*/)`

And finally in terminal `node index.js`
