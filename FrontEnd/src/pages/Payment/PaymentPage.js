import React from 'react';
import PaymentForm from './PaymentForm';
// import { reset } from 'redux-form';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import axios from 'axios';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import getConfig from '../../getConfig';
import {connectWithStyle} from '../../utils.js'

const {router} = getConfig()
const styles = theme => {
    const paddingLeft = theme.normalPadding+'px'
    const style = {
        paddingLeft: {paddingLeft}
    }
    return style 
}


function SimpleDialog(props) {
  const { onClose,message, ...other } = props;

  return (
    <Dialog aria-labelledby="simple-dialog-title" {...other}>
      <DialogTitle id="simple-dialog-title">Payment status</DialogTitle>
      <DialogContent>{message}

      </DialogContent>
      <Button variant="contained" onClick={onClose} color="primary" >
           Back
      </Button>
    </Dialog>
  );
}



export class PaymentPage extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.sendMyCard = this.sendMyCard.bind(this);
        this.state = { 
            notify: false,
            message: '',
            error: false,
            dialogOpen: false,
         };
    }

    sendMyCard(values) {
        console.log(values)
        // console.log(this.props.formVal)
        const sendableFormData = new FormData();
        sendableFormData.append('data', JSON.stringify({
            input_params: values
        }))
        axios.post(router.pay, sendableFormData, {
            headers: {
                'Content-Type': 'multipart/form-data',
            },
            withCredentials: true,
        }).then(function (json_res) {
            console.log(json_res);
            return json_res
        }).then((json_res)=>{
            // alert(json_res)
            this.openDialog(json_res.data.message)
            // this.redirectToProjectInfoList()
        }).catch((error) => {
            console.error('the error is')
            console.error(error.response)
            let mess = 'unknow error'
            try{
                mess = error.response.data.message
            }catch(e){}
           this.openDialog(mess)
            console.log(error);
        });
    }

    handleClose(){
        this.setState({dialogOpen : false})
    }

    openDialog(message){
        this.setState({dialogOpen : true, message})
    }

    render() {
        const st = this.state
        return (
            <div>
                <Card>
                    <CardContent>
                        <h1>Payment page</h1>
                    </CardContent>
                    <PaymentForm
                        onSubmit={this.sendMyCard}
                        onCancel={() => this.redirectToProjectInfoList()}
                        editMode
                    />
                </Card>
                <span onClick={() => this.openDialog() } > </span>
                <SimpleDialog open={st.dialogOpen} message={st.message} onClose={() => this.handleClose()} ></SimpleDialog>
            </div>
        );
    }
}

function mapStateToProps(state, ownProps) {
    const id = ownProps.match.params.id;
    const form = state.form.reduxForm_srtCorrector
    return {
        id: id,
        formVal: form && form.values
    };
}

export default connectWithStyle({comp: PaymentPage, mapStateToProps, styles});
