# [Carma_server_test](https://gitlab.com/bormat2/carma_server) &middot; [![GitHub license](https://img.shields.io/badge/license-MIT-blue.svg)](#)

# Installation
If you just want to use the project follow these instructions
https://www.npmjs.com/package/carma_test0_server

Otherwise, just follow previous instructions to create the database and then follow below instructions

# How to fork the project
## Get the project
`git clone https://gitlab.com/bormat2/carmatest.git`
## Install dependencies
  You need postgresSQL 11 and Node 12

  In FrontEnd folder and in the BackEnd folder run :

`npm i`
## Development
To run react open the directory FrontEnd in your terminal and then run:

`npm start`

go to http://localhost:3000/

but ajax request will not works if you don't launch the node server

open a new terminal without closing the old one, go to BackEnd directory and run

`npm start`

the server will listen to Ajax requests on port 5555

WARNING: For the moment, the code in React is watched, and React server will restart if you make changes, but if you change the code in the BackEnd folder, you have to stop the server and run npm start again.

To run tests containing ".spec." in their name:  

`npm run test`

## Build the code to publish it on npm

Firstly you have to build react, go to the folder FrontEnd and run:
`npm run build`

then go to BackEnd folder (Do not do this before building React) and run :
`npm run build_start_prod`

check everything is ok by running by going to http://localhost:5555

then publish on NPM, still in the BackEnd folder, run :
`npm publish` (you have to be logged with `npm login` if you have never used it)

# Explanation of my choices

## card encryption
I have chosen the library require('crypto') with the aes-256-ctr.
This algorithm is a symmetric-key algorithm, meaning we can decrypt with the same key we used to encrypt and it is what we need here.

Here the key to code and decode is directly in the javascript but it should be in an external file that is not saved on git for better security.

## Luhn Algorithm
I have simply copy and paste the javascript function that is on Wikipedia, because I don't think that adding a dependency for 10 lines of code that will never change is a good idea.


# TODO

TODO LIST / Problems:
  * Once open, left panel can't be closed with the arrow, you have to click on the hamburger button to close it
  * Ports can't be configured you need to have 5555 and 3000 free
  * Database user/password and port can not be changed
  * some files are the result of build scripts and should be in gitIgnore
  * the title of the page has not been customized
  * correct warning about "Buffer" that should me Buffer.alloc()
  * some variables are duplicated in FrontEnd and BackEnd and can be refactored
  * add a watch script in the BackEnd folder
